# Naming conventions

### Components

Components start with a `c-` prefix followed by the component name (ex. `c-button`).

## Objects

Components start with a `c-` prefix followed by the object name (ex. `c-button`).

## Layouts

Layouts start with a `l-` prefix followed by the layout name (ex. `l-header`).

# Folder structure

## Assets

## Includes

## Languages

## Page Templates

## Plugins

## SRC

The `src' folder holds all the native files for the WordPress theme. It's important to notice that the`src` folder will not be shipped with the WordPress theme.

### fonts

The `fonts` folder contains the native web fonts which will be compressed and copied to the `àssets/fonts` folder for production.

### scss

- **abstracts**
  - \_functions.scss - Project functions
  - \_mixins.scss - Project mixins
  - \_placeholders.scss - Project placeholders
  - \_variables.scss - Project variables
- **base**
- \_base.scss

## Template Paths

### .browserslistrc
