<?php get_header(); ?>
<section>
	<?php if ( have_posts() ) : ?>
		<?php
		the_archive_title( '<h1>', '</h1>' );
		the_archive_description();
		?>
		<?php
		while ( have_posts() ) :
			the_post();
			?>
			<?php // IDEA: Move this to a get_template_part() ?>
	  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<h2>
		  <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
		</h2>
		<div class="c-flex-rows c-post-meta">
		  <div class="u-flex-auto">
			<span class="author"><?php _e( 'Published by', 'theme-boilerplate' ); ?> <?php the_author_posts_link(); ?></span>
		  </div>
		  <div>
			<?php if ( comments_open() === true ) { ?>
			<a href="<?php echo get_permalink(); ?>#comments"> <?php echo get_comments_number(); ?> <?php _e( 'comments', 'theme-boilerplate' ); ?></a>
			<?php } ?>
		  </div>
		</div>
			<?php the_excerpt(); ?>
		<p><a href="<?php the_permalink(); ?>" class="c-button c-button"><?php _e( 'Read more', 'theme-boilerplate' ); ?></a></p>
	  </article>
		<?php endwhile ?>
	<?php else : ?>
		<?php // TODO: Make the text dynamic. Please take multilingual in consideration. ?>
		<?php // IDEA: Create a function for this to use globally ?>
	  <h3><?php _e( 'Bummer', 'theme-boilerplate' ); ?></h3>
	  <p><?php _e( 'We don\'t have any information to show you.', 'theme-boilerplate' ); ?></p>
		<?php // IDEA: Add textual information for the user ?>
		<?php // IDEA: Add a search form ?>
	<?php endif; ?>
</section>
<?php get_footer(); ?>
