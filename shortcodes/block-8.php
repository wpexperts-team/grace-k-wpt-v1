<?php

namespace Grace_K_WPT;

class Shortcode_Block_8
{
	public function __construct()
	{
		add_shortcode('gk-3-images', [$this, 'output']);
	}
	public function output($atts)
	{
		$atts = shortcode_atts(
			array(
				'ls_large'     => null,
				'ls_small'     => null,
				'pt'     => null,
				'style'     => null,
			),
			$atts,
			'gk-3-images'
		);
		if ($atts['ls_large'] && $atts['ls_small'] && $atts['pt']) :
			ob_start();
?>
			<div class="c-block-8">
				<div class="c-block-8__item-1">
					<img src="<?php echo wp_get_attachment_image_src($atts['pt'], '4_3_portrait_large')[0] ?>" alt="" class="c-block-8__image c-block-8__image--portrait">
				</div>
				<div class="c-block-8__item-2">
					<img src="<?php echo wp_get_attachment_image_src($atts['ls_large'], '4_3_landscape_large')[0] ?>" alt="" class="c-block-8__image c-block-8__image--landscape">
					<img src="<?php echo wp_get_attachment_image_src($atts['ls_small'], '4_3_landscape_large')[0] ?>" alt="" class="c-block-8__image c-block-8__image--landscape-small">
				</div>
			</div>
<?php
			$output = ob_get_contents();
			ob_end_clean();
			return $output;
		endif;
	}
}
