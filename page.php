<?php get_header(); ?>

<?php
$args = array(
	'post_type' => 'kind-word',
	'posts_per_page' => -1
);
$get_posts = get_posts($args);
// echo '<pre>';
// echo print_r( $posts );
// echo '</pre>';
// die();
?>

<?php if (have_posts()) : ?>
	<?php
	while (have_posts()) :
		the_post();
	?>
	<div class="c-editor">
		<?php the_content(); ?>
	</div>
	<?php endwhile ?>
<?php else : ?>
	<!-- Do Nothing -->
<?php endif; ?>

<?php get_footer(); ?>
