<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

define( 'GRACE_K_WPT', 'wpx-theme-name' );
define( 'GRACE_K_WPT_VERSION', '1.0.0' );
define( 'GRACE_K_WPT_URL', get_stylesheet_directory_uri() );
define( 'GRACE_K_WPT_PATH', get_stylesheet_directory() );

if ( ! isset( $content_width ) ) {
	$content_width = 800;
}

if ( ! class_exists( 'Grace_K_WPT_Setup' ) ) {

	class Grace_K_WPT_Setup {


		public function __construct() {
			add_action( 'after_setup_theme', array( $this, 'theme_setup' ), 99 );
			$this->theme_admin();
			$this->theme_public();
			add_action('enqueue_block_assets', [$this, 'enqueue_block_assets']);
			add_action('enqueue_block_editor_assets', [$this, 'enqueue_block_editor_assets']);
			require 'shortcodes/block-8.php';
			$shortcode_block_8 = new \Grace_K_WPT\Shortcode_Block_8();
			// require_once 'plugins/gravityforms/gravityforms.php';
			// require_once 'plugins/wordpress-seo/wordpress-seo.php';
		}

		public function theme_setup() {
			load_theme_textdomain( 'grace-k', get_template_directory() . '/languages' );
			add_theme_support( 'automatic-feed-links' );
			add_theme_support( 'post-thumbnails' );
			add_theme_support( 'menus' );
			add_theme_support( 'widgets' );
			add_theme_support( 'title-tag' );
			add_theme_support(
				'html5',
				array(
					'comment-list',
					'comment-form',
					'search-form',
					'gallery',
					'caption',
				)
			);
			add_theme_support(
				'custom-logo',
				array(
					'height'      => 100,
					'width'       => 400,
					'flex-height' => true,
					'flex-width'  => true,
					'header-text' => array( 'site-title', 'site-description' ),
				)
			);
			// Add theme support for selective refresh for widgets
			add_theme_support( 'customize-selective-refresh-widgets' );
			add_theme_support('align-wide');
			add_theme_support( 'responsive-embeds' );
			// Add support for block styles
			// add_theme_support( 'wp-block-styles' );
			add_theme_support('editor-styles');
			add_post_type_support( 'page', 'excerpt' );
			add_theme_support( 'disable-custom-font-sizes' );
			// Enqueue editor styles
			add_editor_style( 'assets/css/editor-style.css' );
		}

		public function theme_admin() {
			include_once 'includes/class-theme-admin.php';
			$admin = new \Grace_K_WPT\Theme_Admin();
		}

		public function theme_public() {
			include_once 'includes/class-theme-public.php';
			$public = new \Grace_K_WPT\Theme_Public();
		}

		public function enqueue_block_assets() {
			// wp_enqueue_script(
			// 	'grace-k-wpt-blocks-js',
			// 	GRACE_K_WPP_URL . '/assets/js/editor.blocks.js',
			// 	['wp-i18n', 'wp-element', 'wp-blocks', 'wp-components', 'wp-editor'], ''
			// );
			wp_enqueue_style(
				'grace-k-wpt-blocks-style',
				GRACE_K_WPT_URL . '/assets/css/blocks-style.css',
				null, ''
			);
		}

		public function enqueue_block_editor_assets() {
			wp_enqueue_style(
				'grace-k-wpt-blocks-editor-style',
				GRACE_K_WPT_URL . '/assets/css/blocks-editor-style.css',
				array('grace-k-wpt-blocks-style'), ''
			);
		}
	}
}

new Grace_K_WPT_Setup();
