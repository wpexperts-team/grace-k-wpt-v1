<?php get_header(); ?>

<h1><?php _e( 'Zoekresultaten', 'theme-boilerplate' ); ?></h1>
	<?php printf( __( 'Zoekresultaten voor: %s', 'theme-boilerplate' ), '<span>' . get_search_query() . '</span>' ); ?>
	<p><?php get_search_form(); ?></p>
	<?php if ( have_posts() ) : ?>
		<?php
		while ( have_posts() ) :
			the_post();
			?>
			<?php // IDEA: Move this to a get_template_part() ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php the_title( '<h2>', '</h2>' ); ?>
			<?php the_excerpt(); ?>
		</article>
		<?php endwhile ?>
		<?php else : ?>
		<!-- Do Nothing -->
	<?php endif; ?>

<?php get_footer(); ?>
