<?php get_header();

?>

<?php if ( have_posts() ) : ?>
	<?php
	while ( have_posts() ) :
		the_post();
		?>

		<?php // IDEA: Move this to a get_template_part() ?>
	<section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php the_title( '<h1>', '</h1>' ); ?>
		<?php the_content(); ?>
		Lorem ipsum dolor sit amet <a href="">consectetur adipisicing elit</a>. Dolore harum iure esse culpa odio eligendi sunt, vel aliquam tempore corrupti dolorum. Repudiandae quia sint maiores corrupti ea, itaque aperiam harum quaerat consequatur. Magnam excepturi quod hic nostrum! Vero, saepe praesentium!</p>
		<p>Molestiae iusto qui molestias optio, necessitatibus quos maxime! Adipisci, dignissimos repellendus. Ipsum numquam doloribus praesentium dolorem natus vero dolore quidem error assumenda eum, unde sequi quo expedita, consequuntur quos animi dignissimos ratione sunt repellat dolores aut! Repellendus natus sint perferendis.</p>
		<p>Officiis magnam illo fugit culpa minima possimus qui, nihil nulla. Repellendus necessitatibus quos quis atque autem quasi animi quo exercitationem nesciunt sequi, modi fugiat possimus odit consequuntur architecto veritatis unde odio expedita minima! Perferendis vitae dicta voluptas. Eligendi, possimus fugit!</p>
		<p>Error quod ea totam placeat iusto distinctio in ex, doloribus repellat voluptate modi eum quas consequuntur temporibus, optio non. Minima ratione quasi, nisi fugiat quam maiores optio odit commodi, voluptatem, reprehenderit natus alias? Quibusdam iusto nemo, cumque ea perspiciatis placeat?</p>
		<p>Quaerat provident minima magni quis dolorem qui enim libero, quisquam eos labore quam, quo commodi necessitatibus quidem neque nisi debitis praesentium repudiandae, aliquid adipisci nemo atque. Temporibus ducimus ipsam consequuntur repellat at vitae veritatis eum! Voluptatibus quos ipsa quas! Voluptatibus.</p>
		<?php the_post_navigation(); ?>
	</section>
	<div>
		<form action="">
			<ul>
				<li>Text: <input type="text" name="" id="" placeholder="Enter your text"></li>
				<li><input type="number" name="" id="" placeholder="Enter your text"></li>
				<li><input type="password" name="" id="" placeholder="Enter your text"></li>
				<li><input type="email" name="" id="" placeholder="Enter your text"></li>
				<li><input type="tel" name="" id="" placeholder="Enter your text"></li>
				<li><input type="search" name="" id="" placeholder="Enter your text"></li>
				<li><input type="url" name="" id="" placeholder="Enter your text"></li>
				<li><textarea name="" id="" cols="30" rows="10" placeholder="Enter your text"></textarea></li>
			</ul>
		</form>
		<p style="color:blue">Lorem ipsum dolor sit amet <a href="">consectetur</a>, adipisicing elit. Distinctio, praesentium! Odio officia omnis laudantium, possimus earum assumenda perferendis repudiandae sint?</p>
		<ul>
			<li>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aspernatur deleniti aut, natus voluptas exercitationem voluptates, reiciendis laudantium animi optio aliquid commodi facilis cupiditate sunt laborum consequuntur eveniet! Dolore, quam animi.</li>
			<li>Rem laudantium explicabo pariatur accusamus vel id dolores possimus earum sed eligendi officiis neque ducimus mollitia voluptatem delectus, minima non, soluta quaerat consequatur veritatis inventore atque. Accusantium eos laboriosam placeat.</li>
			<li>Labore pariatur in voluptas earum, sint cumque nemo commodi tempora. Autem similique molestiae, earum ratione qui error odit! Quidem et doloremque aut necessitatibus ad magnam laboriosam totam eos debitis assumenda!</li>
			<li>Facilis optio pariatur impedit expedita nemo? Possimus quidem quibusdam eum ea dolores. Sequi mollitia deserunt cum ea exercitationem, illo ullam. Omnis, ea assumenda! Magni culpa quam dolore fugiat explicabo ipsum!</li>
			<li>Alias doloribus quasi, illum at dolore pariatur blanditiis dignissimos modi maiores non! Doloribus, expedita nihil. Corrupti assumenda tenetur similique, in temporibus ullam neque repellendus eaque minus facilis ducimus ea aliquid.</li>
		</ul>
	</div>
	<?php endwhile ?>
	<?php else : ?>
	<!-- Do Nothing -->
	<?php endif; ?>

<?php get_footer(); ?>
