<footer>
	<div class="c-footer-block-2">
		<div class="c-container-max-width">
			<div class="c-footer-block-1">
				<div class="c-footer-block-1__item">
					<div class="c-footer-contact-info">
						<h4 class="c-footer-contact-info__name">Grace K</h4>
						PAKT - Lamorinièrestraat 161B<br/>
						2018 Antwerpen<br/>
						E: <a href="mailto:info@gracek.be">info@gracek.be</a>
					</div>

				</div>
				<div class="c-footer-block-1__item">
					<?php
					$wp_nav_menu = array(
						'theme_location'  => 'footer-nav-menu-primary',
						'menu'            => '',
						'container'       => 'nav',
						'container_class' => 'c-footer-nav-menu-primary',
						'container_id'    => '',
						'menu_class'      => '',
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => '',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul id="%1$s" class="c-footer-nav-menu-primary-list">%3$s</ul>',
						'depth'           => 1,
						'walker'          => '',
					);
					wp_nav_menu($wp_nav_menu);
					?>
				</div>
				<div class="c-footer-block-1__item c-footer-block-1__item--align-right">
					<div class="c-footer-block-1__item-layout">
						<div class="c-footer-block-1__item-layout-item">
							<ul class="c-footer-social-media-list">
								<li class="c-footer-social-media-list__item"><a href="#"><i class="fab fa-facebook-square"></i></a></li>
								<li class="c-footer-social-media-list__item"><a href="#"><i class="fab fa-instagram"></i></a></li>
								<li class="c-footer-social-media-list__item"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
						<div class="c-footer-block-1__item-layout-item c-footer-block-1__item-layout-item--last-item">
							<?php
							$wp_nav_menu = array(
								'theme_location'  => 'footer-nav-menu-secondary',
								'menu'            => '',
								'container'       => 'nav',
								'container_class' => 'c-footer-nav-menu-secondary',
								'container_id'    => '',
								'menu_class'      => '',
								'menu_id'         => '',
								'echo'            => true,
								'fallback_cb'     => '',
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul id="%1$s" class="c-footer-nav-menu-secondary-list">%3$s</ul>',
								'depth'           => 1,
								'walker'          => '',
							);
							wp_nav_menu($wp_nav_menu);
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
