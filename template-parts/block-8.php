<div class="c-block-8">
	<div class="c-block-8__item-1">
		<img src="https://source.unsplash.com/600x800/?event&sig=43123458" alt="" class="c-block-8__image c-block-8__image--portrait">
	</div>
	<div class="c-block-8__item-2">
		<img src="https://source.unsplash.com/800x600/?event&sig=43123434" alt="" class="c-block-8__image c-block-8__image--landscape">
		<img src="https://source.unsplash.com/800x600/?event&sig=431234SZ" alt="" class="c-block-8__image c-block-8__image--landscape-small">
	</div>
</div>
