<header>
	<div class="c-mobile-nav">
		<div class="c-mobile-nav__logo">
			<a href="<?php echo get_home_url() ?>"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/logo-grace-k.svg' ?>" alt="Grace K" class="c-mobile-nav__logo-image"></a>
		</div>
		<div class="c-mobile-nav__menu">
			<a href="#" class="c-mobile-nav__menu-link"><i class="fas fa-bars c-mobile-nav__menu-icon"></i></a>

		</div>
	</div>
	<div class="c-mobile-nav-body">
		<div class="c-mobile-nav-body__content">
			<?php
			$wp_nav_menu = array(
				'theme_location'  => 'header-nav-menu-primary',
				'menu'            => '',
				'container'       => 'nav',
				'container_class' => 'c-mobile-nav-menu-primary',
				'container_id'    => '',
				'menu_class'      => '',
				'menu_id'         => '',
				'echo'            => true,
				'fallback_cb'     => '',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul id="%1$s" class="c-mobile-nav-menu-primary__list">%3$s</ul>',
				'depth'           => 1,
				'walker'          => '',
			);
			wp_nav_menu($wp_nav_menu);
			?>
		</div>
	</div>
	<script type="text/javascript">
		jQuery(function($) {
			$(".c-mobile-nav__menu-link").click(function(){
				$(".c-mobile-nav-body").slideToggle();
				$(".c-mobile-nav-body").css("display","grid");
				//$(".c-mobile-nav-icon").toggleClass("c-mobile-nav-icon--active");
			});
			$(".c-mobile-nav-menu-primary__list > li > a").click(function(){
				$(".c-mobile-nav-body").slideToggle();
				$(".c-mobile-nav-body").css("display","none");
			});
		});
	</script>
	<div class="c-container">
		<div class="c-container-max-width">
			<div class="c-header-block-1">
				<div class="c-header-block-1__logo">
					<a href="<?php echo get_home_url() ?>"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/logo-grace-k.svg' ?>" alt="Grace K"></a>
				</div>
				<div class="c-header-block-1__nav">
					<?php
					$wp_nav_menu = array(
						'theme_location'  => 'header-nav-menu-primary',
						'menu'            => '',
						'container'       => 'nav',
						'container_class' => 'c-header-block-2',
						'container_id'    => '',
						'menu_class'      => '',
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => '',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul id="%1$s" class="c-header-block-2__list">%3$s</ul>',
						'depth'           => 1,
						'walker'          => '',
					);
					wp_nav_menu($wp_nav_menu);
					?>
				</div>
			</div>
		</div>
	</div>
</header>
