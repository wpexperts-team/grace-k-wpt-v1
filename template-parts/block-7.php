<div class="c-container">
	<div class="c-container-max-width c-container-max-width--xl">
		<div class="c-block-7">
			<div class="c-block-7__content">
				<div class="c-block-7__content-wrapper">
					<div class="c-block-7__letter"><?php echo $atts['letter']  ?></div>
				</div>
			</div>
			<div class="c-block-7__media">
				<img src="https://source.unsplash.com/800x300/?event&sig=<?php echo uniqid() ?>" alt="" class="c-block-7__image">
			</div>
		</div>
	</div>
</div>
