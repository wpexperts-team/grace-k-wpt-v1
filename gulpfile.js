const siteUrl = "https://localhost";

// Load plugins
const autoprefixer = require("autoprefixer");
const browserSync = require("browser-sync").create();
const gulp = require("gulp");
const plumber = require("gulp-plumber");
const postcss = require("gulp-postcss");
const rename = require("gulp-rename");
const sass = require("gulp-sass");
const sourcemaps = require("gulp-sourcemaps");
const changed = require("gulp-changed");
const cleanCSS = require("clean-css");
const cssnano = require("cssnano");
const imagemin = require("gulp-imagemin");

// Javascript
const srcJS = "";
const distJS = "";

// Sass
const srcSass = "./src/sass/**/*.scss";
const distSass = "./assets/css/";

// PHP
const srcPHP = "./**/*.php";

// Images
const srcImages = "./src/images/*";
const distImages = "./assets/images/";

// Fonts
const srcFonts = "./src/fonts/*";
const distFonts = "./assets/fonts/";

// BrowserSync
function browserSyncServe(done) {
  browserSync.init({
    open: "external",
    proxy: siteUrl,
    port: "3000"
  });
  done();
}

// BrowserSync Reload
function browserSyncReload(done) {
  browserSync.reload();
  done();
}

function fontawesomeWebfonts() {
  return gulp
    .src("./node_modules/@fortawesome/fontawesome-free/webfonts/*")
    .pipe(gulp.dest("./assets/vendors/fortawesome/fontawesome-free/webfonts/"));
}

function fontawesomeCSS() {
  return gulp
    .src("./node_modules/@fortawesome/fontawesome-free/css/*")
    .pipe(gulp.dest("./assets/vendors/fortawesome/fontawesome-free/css/"));
}

// CSS task
function css() {
  return (
    gulp
      .src(srcSass)
      //.pipe(plumber())
      .pipe(
        sourcemaps.init({
          loadMaps: true,
          largeFile: true
        })
      )
      .pipe(
        sass({
          outputStyle: "expanded"
        })
      )
      .on("error", sass.logError)
      .pipe(postcss([autoprefixer()]))
      //.pipe(cleanCSS())
      .pipe(sourcemaps.write(""))
      .pipe(gulp.dest(distSass))
      .pipe(browserSync.stream())
  );
}

function cssMinified() {
  return (
    gulp
      .src(srcSass)
      //.pipe(plumber())
      .pipe(
        sourcemaps.init({
          loadMaps: true,
          largeFile: true
        })
      )
      .pipe(
        sass({
          outputStyle: "compressed"
        })
      )
      .on("error", sass.logError)
      .pipe(
        rename({
          suffix: ".min"
        })
      )
      .pipe(postcss([autoprefixer(), cssnano()]))
      //.pipe(cleanCSS())
      .pipe(sourcemaps.write(""))
      .pipe(gulp.dest(distSass))
      .pipe(browserSync.stream())
  );
}

function images() {
  return gulp
    .src(srcImages)
    .pipe(changed(distImages))
    .pipe(
      imagemin([
        imagemin.gifsicle({
          interlaced: true
        }),
        imagemin.jpegtran({
          progressive: true
        }),
        imagemin.optipng({
          optimizationLevel: 5
        })
      ])
    )
    .pipe(gulp.dest(distImages));
}

function fonts() {
  return gulp.src(srcFonts).pipe(gulp.dest(distFonts));
}

// Watch files
function watch() {
  gulp.watch(srcSass, gulp.series(css, cssMinified, browserSyncReload));
  gulp.watch(srcImages, gulp.series(images));
  gulp.watch(srcFonts, gulp.series(fonts));
  gulp.watch(srcPHP, gulp.series(browserSyncReload));
}

// Export tasks
exports.watch = watch;
exports.css = css;
exports.cssMinified = cssMinified;
exports.images = images;
exports.fonts = fonts;
exports.fontawesomeWebfonts = fontawesomeWebfonts;
exports.fontawesomeCSS = fontawesomeCSS;

let build = gulp.parallel(watch, browserSyncServe);
gulp.task("default", build);
