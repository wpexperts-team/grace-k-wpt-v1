<?php get_header(); ?>

<div class="c-container">
	<div class="c-container-max-width c-container-max-width--xl">
		<div class="c-block-2">
			<img src="https://source.unsplash.com/800x600/?event&sig=43123458" alt="" class="c-block-2__image">
		</div>
	</div>
</div>

<div class="c-container">
	<div class="c-container-max-width">
		<hr>
		<!-- Client -->
		<?php
		$client = get_field('story_client');
		if ($client) {
			echo '<p>' . __('Client', 'grace-k') . ': ' . $client . '</p>';
		}
		?>
		<!-- Teaser Video Vimeo -->
		<?php
		$teaser_video_id = get_field('story_teaser_video_vimeo_id');
		echo '<p>' . __('Teaser Video Vimeo ID', 'grace-k') . ': ' . $teaser_video_id . '</p>';
		if ($teaser_video_id) {
		?>
			<iframe src="https://player.vimeo.com/video/<?php echo $teaser_video_id; ?>" width="600" height="400" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			<script src="https://player.vimeo.com/api/player.js"></script>
			<script>
			</script>
		<?php
		}
		?>
		<!-- Gallery -->
		<?php
		$images = get_field('story_gallery');
		if ($images) :
		?>
			<h3 class="c-text-style-5">Gallery</h3>
			<ul>
				<?php foreach ($images as $image) : ?>
					<li>
						<a href="<?php echo esc_url($image['url']); ?>">
							<img src="<?php echo esc_url($image['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
						</a>
						<p><?php echo esc_html($image['caption']); ?></p>
					</li>
				<?php endforeach; ?>
			</ul>
		<?php endif ?>
	</div>
</div>

<div class="c-container">
	<div class="c-block-8">
		<div class="c-block-8__item-1">
			<img src="https://source.unsplash.com/600x800/?event&sig=43123458" alt="" class="c-block-8__image c-block-8__image--portrait">
		</div>
		<div class="c-block-8__item-2">
			<img src="https://source.unsplash.com/800x600/?event&sig=43123434" alt="" class="c-block-8__image c-block-8__image--landscape">
			<img src="https://source.unsplash.com/800x600/?event&sig=431234SZ" alt="" class="c-block-8__image c-block-8__image--landscape-small">
		</div>
	</div>
</div>

<div class="c-container" style="margin: 8rem 0">
	<div class="c-container-max-width c-container-max-width--sm" style="text-align:center">
		<h2 class="c-text-style-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>
		<p class="c-text-style-1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Id, beatae quia repellat quisquam alias expedita. At vel facere tempora neque, delectus accusantium fuga excepturi temporibus itaque consequatur minima omnis quaerat.</p>
		<p>Autem sunt ab magnam? Minus assumenda unde, commodi accusantium eos expedita pariatur eum voluptas nihil consequatur quae consequuntur reiciendis fugit, nemo error quidem est laborum corporis nam praesentium esse sint!</p>
	</div>
</div>

<div class="c-container">
	<div class="c-block-8 c-block-8--reverse">
		<div class="c-block-8__item-2 c-block-8__item-2--reverse">
			<img src="https://source.unsplash.com/800x600/?event&sig=43123SAX" alt="" class="c-block-8__image c-block-8__image--landscape">
			<img src="https://source.unsplash.com/800x600/?event&sig=43123" alt="" class="c-block-8__image c-block-8__image--landscape-small">
		</div>
		<div class="c-block-8__item-1">
			<img src="https://source.unsplash.com/600x800/?event&sig=43123DZ3" alt="" class="c-block-8__image c-block-8__image--portrait">
		</div>
	</div>
</div>

<div class="c-container" style="margin: 8rem 0">
	<div class="c-container-max-width c-container-max-width--sm" style="text-align:center">
		<h2 class="c-text-style-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>
		<p class="c-text-style-1" style="color: --color-secondary">Lorem ipsum dolor sit amet consectetur adipisicing elit. Id, beatae quia repellat quisquam alias expedita. At vel facere tempora neque, delectus accusantium fuga excepturi temporibus itaque consequatur minima omnis quaerat.</p>
		<p>Autem sunt ab magnam? Minus assumenda unde, commodi accusantium eos expedita pariatur eum voluptas nihil consequatur quae consequuntur reiciendis fugit, nemo error quidem est laborum corporis nam praesentium esse sint!</p>
	</div>
</div>

<div class="c-container">
	<div class="c-block-8">
		<div class="c-block-8__item-1">
			<img src="https://source.unsplash.com/600x800/?event&sig=43123457" alt="" class="c-block-8__image c-block-8__image--portrait">
		</div>
		<div class="c-block-8__item-2">
			<img src="https://source.unsplash.com/800x600/?event&sig=43123433" alt="" class="c-block-8__image c-block-8__image--landscape">
			<img src="https://source.unsplash.com/800x600/?event&sig=431234SA" alt="" class="c-block-8__image c-block-8__image--landscape-small">
		</div>
	</div>
</div>

<?php get_footer(); ?>
