<form class="c-search" method="get" action="<?php echo home_url(); ?>" role="search">
	<input class="c-search__input" type="search" name="s" placeholder="<?php _e( 'To search, type and hit enter.', 'theme-boilerplate' ); ?>">
	<button class="c-search__button c-button" type="submit" role="button"><?php _e( 'Search', 'theme-boilerplate' ); ?></button>
</form>
