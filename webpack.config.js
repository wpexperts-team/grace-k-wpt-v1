const MiniCssExtractPluginStyle = require("mini-css-extract-plugin");
const MiniCssExtractPluginEditorStyle = require("mini-css-extract-plugin");
const path = require("path");
const BrowserSyncPlugin = require("browser-sync-webpack-plugin");

module.exports = {
  entry: {
    "./assets/css/style": "./src/sass/style.scss",
    "./assets/css/editor-style": "./src/sass/editor-style.scss",
    "./assets/css/blocks-style": "./src/sass/blocks-style.scss",
    "./assets/css/blocks-editor-style": "./src/sass/blocks-editor-style.scss"
  },
  output: {
    // filename: "[name].js",
    path: path.resolve(__dirname)
  },
  devtool: "inline-cheap-source-map",
  module: {
    rules: [
      {
        test: /\.scss$/,
        exclude: ["/node_modules"],
        use: [
          "style-loader",
          MiniCssExtractPluginStyle.loader,
          {
            loader: "css-loader",
            options: {
              url: false,
              sourceMap: true
            }
          },
          {
            loader: "postcss-loader",
            options: {
              plugins: [require("autoprefixer")]
            }
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ["file-loader"]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPluginStyle({
      // filename: "./assets/css/style.css",
      // chunkFilename: "./assets/css/style.css"
    })
    // new BrowserSyncPlugin({
    //   files: "**/*",
    //   host: "localhost",
    //   port: 3000,
    //   proxy: "http://localhost",
    //   watchOptions: {
    //     ignoreInitial: true,
    //     ignored: ["node_modules"]
    //   }
    // })
  ]
};
