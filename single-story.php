<?php get_header(); ?>

<?php if (have_posts()) : ?>
	<?php
	while (have_posts()) :
		the_post();
	?>
		<section class="c-editor">
			<?php // the_title( '<h1 class="c-text-style-6" style="text-align:center">', '</h1>' ); ?>
			<?php the_content(); ?>
		</section>
	<?php endwhile ?>
<?php else : ?>
	<!-- Do Nothing -->
<?php endif; ?>

<?php get_footer(); ?>
