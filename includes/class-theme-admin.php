<?php
namespace Grace_K_WPT;

class Theme_Admin {

	public function __construct() {
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		// add_action( 'admin_enqueue_scripts', array( $this, 'add_hubspot_chat' ) );
		add_filter( 'image_size_names_choose', array( $this, 'set_custom_image_sizes' ) );
		add_action( 'after_setup_theme', array( $this, 'after_setup_theme' ) );
		$this->set_term_description_shortcodes();
		$this->set_excerpt_shortcodes();
		$this->set_comments_shortcodes();
		// add_action( 'enqueue_block_editor_assets', [$this, 'load_block_files'] );
	}

	public function enqueue_styles() {
		// wp_enqueue_style( 'admin-styles', GRACE_K_WPT_URL . '/admin/assets/css/styles.css' );
	}

	public function load_block_files() {
		// wp_enqueue_script( 'wp-blocks-test', GRACE_K_WPT_URL . '/assets/js/wp-blocks.js', ['wp-blocks', 'wp-i18n', 'wp-editor'], true );
	}

	public function enqueue_scripts() {
	}

	public function after_setup_theme() {
		$this->add_image_sizes();
		$this->register_nav_menus();
		add_theme_support( 'editor-color-palette', array(
		array(
			'name'  => __( 'Color Primary', 'grace-k' ),
			'slug'  => 'color-primary',
			'color'	=> '#454545',
		),
		array(
			'name'  => __( 'Color Secondary', 'grace-k' ),
			'slug'  => 'color-secondary',
			'color' => '#e70395',
		),
		array(
			'name'	=> __( 'Color Secondary Tint 1', 'grace-k' ),
			'slug'	=> 'color-secondary-tint-1',
			'color'	=> '#fdf2f9',
		),
	) );
	}

	public function add_image_sizes() {
		add_image_size( 'medium_square', 600, 600, true );
		add_image_size( 'xl', 1750 );
		add_image_size( 'xxl', 2400 );
		// add_image_size('4_3_landscape_medium', 800, 600, true);
		add_image_size('four_three_landscape_large', 1600, 1200, true);
		// add_image_size('4_3_portrait_medium', 600, 800, true);
		// add_image_size('4_3_portait_large', 1200, 1600, true);
		add_image_size('four_three_portrait_large', 1200, 1600, true);
		add_image_size('hero_xs', 2400, 300, true);
		add_image_size('hero_sm', 2400, 600, true);
	}

	public function set_custom_image_sizes( $sizes ) {
		return array_merge(
			$sizes,
			array(
				'medium_square' => __( 'Medium Square', 'grace-k' ),
				'xl'            => __( 'XL', 'grace-k' ),
				'xxl'           => __( 'XXL', 'grace-k' ),
				'four_three_portrait_large'           => __( '4:3 Portrait Large', 'grace-k' ),
				'four_three_landscape_large'           => __( '4:3 Landscape Large', 'grace-k' ),
				'hero_xs'           => __( 'Hero Extra Small', 'grace-k' ),
				'hero_sm'           => __( 'Hero Small', 'grace-k' ),
			)
		);
	}

	/**
	 * Use shortcodes in the_excerpt
	 *
	 * @return void
	 */
	public function set_excerpt_shortcodes() {
		add_filter( 'the_excerpt', 'shortcode_unautop' );
		add_filter( 'the_excerpt', 'do_shortcode' );
	}

	/**
	 * Use shortcodes in categories, tags and taxonomy descriptions.
	 *
	 * @return void
	 */
	public function set_term_description_shortcodes() {
		add_filter( 'term_description', 'shortcode_unautop' );
		add_filter( 'term_description', 'do_shortcode' );
	}

	/**
	 * Use shortcodes in comments
	 *
	 * @return void
	 */
	public function set_comments_shortcodes() {
		add_filter( 'comment_text', 'shortcode_unautop' );
		add_filter( 'comment_text', 'do_shortcode' );
	}

	/**
	 * Register Nav Menus
	 *
	 * @return void
	 */
	public function register_nav_menus() {
		register_nav_menus(
			array(
				'nav-menu-primary'   => __( 'Primary Menu', 'grace-k' ),
				'nav-menu-secondary' => __( 'Secondary Menu', 'grace-k' ),
				// Header
				'header-nav-menu-primary'   => __( 'Header Primary Menu', 'grace-k' ),
				'header-nav-menu-secondary' => __( 'Header Secondary Menu', 'grace-k' ),
				// Footer
				'footer-nav-menu-primary'   => __('Footer Primary Menu', 'grace-k' ),
				'footer-nav-menu-secondary' => __('Footer Secondary Menu', 'grace-k' ),
			)
		);
	}

	public function add_hubspot_chat() {
		?>
		<!-- Start of HubSpot Embed Code -->
  		<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/4155959.js"></script>
		<!-- End of HubSpot Embed Code -->
		<?php
	}
}
