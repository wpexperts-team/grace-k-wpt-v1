<?php

namespace Grace_K_WPT;

class Theme_Public {


	public function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'load_google_fonts' ) );
		// Shortcodes
		add_shortcode( 'gk-block-7', [ $this, 'shortcode_block_7' ] );
	}

	public function enqueue_scripts_styles() {
		// define('SCRIPT_DEBUG', true);
		wp_enqueue_script( 'jquery' );
		wp_enqueue_style( 'theme-styles', GRACE_K_WPT_URL . '/assets/css/style.css', [], GRACE_K_WPT_VERSION );
		$query_args = array(
			'crossorigin' => 'anonymous',
		);
		wp_enqueue_script( 'fontawesome', add_query_arg( $query_args, '//kit.fontawesome.com/157b5abf0d.js' ) );
		// wp_dequeue_style('wp-block-library');
	}

	public function load_google_fonts() {
		$query_args = array(
			'family'       => 'Lato:300,300i,400,400i,700,700i,900|Playfair+Display:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i',
			'font-display' => 'swap',
			'subset'       => 'latin,latin-ext',
		);
		wp_register_style( 'google-fonts', add_query_arg( $query_args, '//fonts.googleapis.com/css' ) );
		wp_enqueue_style( 'google-fonts' );
	}

	public function shortcode_block_7( $atts ) {
		$atts = shortcode_atts(
			array(
				'letter'   => null,
				'image_id' => null,
			),
			$atts
		);
		ob_start();
		include GRACE_K_WPT_PATH . '/template-parts/block-7.php';
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}
}
