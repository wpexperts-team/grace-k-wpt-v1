<?php

// Template Name: Home

get_header();
?>

<?php if ( have_posts() ) : ?>
	<?php
	while ( have_posts() ) :
		the_post();
		?>
		<?php the_content(); ?>
	<?php endwhile ?>
<?php else : ?>
	<!-- Do Nothing -->
<?php endif; ?>

<div class="c-container">
	<div class="c-container-max-width c-container-max-width--xl">
		<div class="c-block-1">
			<div class="c-block-1__item-1">
				<div class="c-block-1__container-1">
					<h1 class="c-text-style-5">Setting The Scene</h1>
					<p>Aangepast ...Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem iure voluptatum quasi recusandae dolores neque animi asperiores esse iusto praesentium amet, dolore, magni sunt atque alias voluptas soluta, corrupti pariatur. Officia, vitae? Veritatis, maxime. Quisquam natus expedita nisi nemo eveniet explicabo! Harum earum commodi minima vitae natus molestiae labore nulla quaerat quo, iste praesentium id, delectus in voluptatum necessitatibus voluptatibus ratione maxime. Nemo unde ullam hic voluptatibus necessitatibus? Enim rerum porro quibusdam nam obcaecati dolore accusantium id provident est laudantium beatae suscipit, natus libero ullam doloremque voluptas aperiam maiores voluptatem debitis! Sed animi unde ratione incidunt fugit ducimus eveniet optio!</p>
				</div>
			</div>
			<div class="c-block-1__item-2">
				Here goes the image
			</div>
		</div>
	</div>
</div>

<div class="c-container">
	<div class="c-container-max-width c-container-max-width--lg">
		<div class="c-container-max-width c-container-max-width--50%">
			<div class="c-block-6">
				<div class="c-block-6__container">
					<h4 class="c-block-6__subtitle">About</h4>
					<h2 class="c-block-6__title">Grace K.</h2>
				</div>
			</div>
			<p class="c-text-style-1 c-text-style-1--color-secondary">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laudantium beatae eius ut! Veritatis quos eius, consequatur iure facilis corrupti voluptate dignissimos, facere deserunt adipisci alias illum dicta commodi unde maxime harum? Suscipit reprehenderit labore atque eos aspernatur rem magnam necessitatibus.</p>
			<p>Distinctio, labore a. Minus, quo explicabo, tempore sint, rem reiciendis beatae quia neque doloremque ut fugit. Aspernatur voluptates voluptatibus, quae obcaecati molestias numquam ipsa, officiis maxime, ex quidem nihil. Natus quaerat inventore ad a facilis dolor laboriosam, alias veniam voluptas quis iste repudiandae, corporis cupiditate pariatur! Repellendus numquam accusantium in qui itaque quidem odio iusto. Eaque saepe fugiat quisquam voluptate.</p>
		</div>
	</div>
</div>

<div class="c-container">
	<div class="c-container-max-width">
		<div class="c-block-6">
			<div class="c-block-6__container">
				<h4 class="c-block-6__subtitle">Let Me Tell You</h4>
				<h2 class="c-block-6__title">Some Stories</h2>
			</div>
		</div>
		<?php
		// Get stories
		$args    = [
			'post_type'      => 'story',
			'posts_per_page' => 6,
			'post_status'    => 'publish',
		];
		$stories = get_posts( $args );
		?>
		<?php
		if ( $stories ) {
			?>
			<div class="c-block-3">
				<?php foreach ( $stories as $story ) { ?>
					<div class="c-block-3__item">
						<a href="<?php echo get_permalink( $story->ID ); ?>" class="c-block-3__link">
							<div class="c-block-4">
								<div class="c-block-4__media">
									<img src="https://source.unsplash.com/800x600/?event?sig=<?php echo $story->ID; ?>" alt="" class="c-block-4__image">
								</div>
								<div class="c-block-4__content">
									<h4 class="c-block-4__subtitle"><?php the_field( 'story_client', $story->ID ); ?></h4>
									<h3 class="c-block-4__title"><?php echo $story->post_title; ?></h3>
									<p><?php echo $story->post_excerpt; ?></p>
								</div>
							</div>
						</a>
					</div>
				<?php } ?>
			</div>
			<?php
		}
		?>
	</div>
</div>

<div class="c-container">
	<div class="c-container-max-width c-container-max-width--xl">
		<div class="c-block-5">
			<div class="c-block-5__item-1">
				<img src="https://source.unsplash.com/800x600/?event&sig=1234" alt="" class="c-block-5__image">
			</div>
			<div class="c-block-5__item-2">
				<div class="c-block-5__container-1">
					<div class="c-block-6">
						<div class="c-block-6__container">
							<h4 class="c-block-6__subtitle">Proud of</h4>
							<h2 class="c-block-6__title">Our Values</h2>
						</div>
					</div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem iure voluptatum quasi recusandae dolores neque animi asperiores esse iusto praesentium amet, dolore, magni sunt atque alias voluptas soluta, corrupti pariatur. Officia, vitae? Veritatis, maxime. Quisquam natus expedita nisi nemo eveniet explicabo! Harum earum commodi minima vitae natus molestiae labore nulla quaerat quo, iste praesentium id, delectus in voluptatum necessitatibus voluptatibus ratione maxime. Nemo unde ullam hic voluptatibus necessitatibus? Enim rerum porro quibusdam nam obcaecati dolore accusantium id provident est laudantium beatae suscipit, natus libero ullam doloremque voluptas aperiam maiores voluptatem debitis! Sed animi unde ratione incidunt fugit ducimus eveniet optio!</p>
					<p><a href="" class="c-button">More info</a></p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="c-container">
	<div class="c-container-max-width c-container-max-width--lg">
		<div class="c-container-max-width" style="margin-top: -4rem;">
			<div class="c-block-6">
				<div class="c-block-6__container">
					<h4 class="c-block-6__subtitle">It's always nice to get</h4>
					<h2 class="c-block-6__title">Kind Words</h2>
				</div>
			</div>
			<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laudantium beatae eius ut! Veritatis quos eius, consequatur iure facilis corrupti voluptate dignissimos, facere deserunt adipisci alias illum dicta commodi unde maxime harum? Suscipit reprehenderit labore atque eos aspernatur rem magnam necessitatibus. Distinctio, labore a. Minus, quo explicabo, tempore sint, rem reiciendis beatae quia neque doloremque ut fugit. Aspernatur voluptates voluptatibus, quae obcaecati molestias numquam ipsa, officiis maxime, ex quidem nihil. Natus quaerat inventore ad a facilis dolor laboriosam, alias veniam voluptas quis iste repudiandae, corporis cupiditate pariatur! Repellendus numquam accusantium in qui itaque quidem odio iusto. Eaque saepe fugiat quisquam voluptate.</p>
		</div>
	</div>
</div>

<div class="c-container">
	<div class="c-container-max-width c-container-max-width--xl">
		<div class="c-block-2">
			<img src="https://source.unsplash.com/800x600/?event&sig=43123458" alt="" class="c-block-2__image">
		</div>
	</div>
	<div class="c-container-max-width c-container-max-width--sm">
		<h2 class="c-text-style-5">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Incidunt, similique!</h2>
		<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas tenetur in sit, sequi, illum facilis voluptatibus, esse sunt deserunt culpa quia sint! Laboriosam quos iste eveniet! Doloremque error sequi molestiae.</p>
		<p>Velit maiores reiciendis ipsa distinctio esse fugit dolore ex, nulla quasi maxime fuga eaque illo ut necessitatibus natus dignissimos consequuntur neque deserunt facere eum. Eaque consequuntur labore veniam voluptate placeat.</p>
		<p>Ab consequatur fugiat accusamus veniam! Voluptatum tenetur distinctio harum cum cupiditate exercitationem consectetur ducimus explicabo eos similique, architecto et impedit culpa vitae optio sit expedita corrupti quos illum ad natus!</p>
	</div>
	<div class="c-container-max-width c-container-max-width--xl">
		<div class="c-block-2">
			<img src="https://source.unsplash.com/800x600/?event&sig=43123458" alt="" class="c-block-2__image c-block-2__image--reverse">
		</div>
	</div>
	<div class="c-container-max-width c-container-max-width--sm">
		<h2 class="c-text-style-5">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Incidunt, similique!</h2>
		<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas tenetur in sit, sequi, illum facilis voluptatibus, esse sunt deserunt culpa quia sint! Laboriosam quos iste eveniet! Doloremque error sequi molestiae.</p>
		<p>Velit maiores reiciendis ipsa distinctio esse fugit dolore ex, nulla quasi maxime fuga eaque illo ut necessitatibus natus dignissimos consequuntur neque deserunt facere eum. Eaque consequuntur labore veniam voluptate placeat.</p>
		<p>Ab consequatur fugiat accusamus veniam! Voluptatum tenetur distinctio harum cum cupiditate exercitationem consectetur ducimus explicabo eos similique, architecto et impedit culpa vitae optio sit expedita corrupti quos illum ad natus!</p>
	</div>
	<div class="c-container-max-width c-container-max-width--xl">
		<div class="c-block-2">
			<img src="https://source.unsplash.com/800x600/?event&sig=43123458" alt="" class="c-block-2__image c-block-2__image--reverse">
		</div>
	</div>
</div>

<div class="c-container">
	<div class="c-container-max-width c-container-max-width--xl">
		<div class="c-block-7">
			<div class="c-block-7__content">
				<div class="c-block-7__content-wrapper">
					<div class="c-block-7__letter">g</div>
				</div>
			</div>
			<div class="c-block-7__media">
				<img src="https://source.unsplash.com/800x300/?event&sig=43123458" alt="" class="c-block-7__image">
			</div>
		</div>
	</div>
	<div class="c-container-max-width c-container-max-width--sm">
		<h2 class="c-text-style-5">Growth</h2>
		<p class="c-text-style-1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas tenetur in sit, sequi, illum facilis voluptatibus, esse sunt deserunt culpa quia sint! Laboriosam quos iste eveniet! Doloremque error sequi molestiae.</p>
		<p>Velit maiores reiciendis ipsa distinctio esse fugit dolore ex, nulla quasi maxime fuga eaque illo ut necessitatibus natus dignissimos consequuntur neque deserunt facere eum. Eaque consequuntur labore veniam voluptate placeat.</p>
		<p>Ab consequatur fugiat accusamus veniam! Voluptatum tenetur distinctio harum cum cupiditate exercitationem consectetur ducimus explicabo eos similique, architecto et impedit culpa vitae optio sit expedita corrupti quos illum ad natus!</p>
	</div>
</div>
<div class="c-container">
	<div class="c-container-max-width c-container-max-width--xl">
		<div class="c-block-7">
			<div class="c-block-7__content">
				<div class="c-block-7__content-wrapper">
					<div class="c-block-7__letter">R</div>
				</div>
			</div>
			<div class="c-block-7__media">
				<img src="https://source.unsplash.com/800x300/?event&sig=431458" alt="" class="c-block-7__image">
			</div>
		</div>
	</div>
	<div class="c-container-max-width c-container-max-width--sm">
		<h2 class="c-text-style-5">Growth</h2>
		<p class="c-text-style-1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas tenetur in sit, sequi, illum facilis voluptatibus, esse sunt deserunt culpa quia sint! Laboriosam quos iste eveniet! Doloremque error sequi molestiae.</p>
		<p>Velit maiores reiciendis ipsa distinctio esse fugit dolore ex, nulla quasi maxime fuga eaque illo ut necessitatibus natus dignissimos consequuntur neque deserunt facere eum. Eaque consequuntur labore veniam voluptate placeat.</p>
		<p>Ab consequatur fugiat accusamus veniam! Voluptatum tenetur distinctio harum cum cupiditate exercitationem consectetur ducimus explicabo eos similique, architecto et impedit culpa vitae optio sit expedita corrupti quos illum ad natus!</p>
	</div>
</div>
<?php get_footer(); ?>
