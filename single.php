<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
	<?php
	while ( have_posts() ) :
		the_post();
		?>
		<?php the_title( '<h1>', '</h1>' ); ?>
		<?php the_content(); ?>
		<?php
		the_post_navigation(
			array(
				'prev_text'          => '%title',
				'next_text'          => '%title',
				'in_same_term'       => false,
				'excluded_terms'     => '',
				'taxonomy'           => 'category',
				'screen_reader_text' => __( 'Post navigation', 'theme-boilerplate' ),
			)
		);
		?>
		<?php
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;
		?>
	<?php endwhile ?>
	<?php else : ?>
	<!-- Do Nothing -->
<?php endif; ?>

<?php get_footer(); ?>
