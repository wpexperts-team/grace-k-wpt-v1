<?php
namespace WP_Experts\Plugin\WordPress_SEO;

if ( is_plugin_active( 'wordpress-seo/wp-seo.php' ) ) {

	class WordPress_SEO {


		public function __construct() {
			add_filter( 'wpseo_metabox_prio', array( $this, 'move_metabox_to_bottom' ) );
			// add_filter( 'wpseo_locale', array( $this, 'change_og_locale_from_wpml_hreflang' ) );
		}

		public function move_metabox_to_bottom() {
			return 'low';
		}

		// public function change_og_locale_from_wpml_hreflang( $locale ) {
		// if( class_exists( 'SitePress' ) ) {
		// $current_language = apply_filters( 'wpml_current_language', NULL );
		// $active_languages = apply_filters( 'wpml_active_languages', NULL );
		// var_dump( $active_languages[$current_language]['default_locale'] );
		// return $active_languages[$current_language]['default_locale'];
		// }
		// }
	}

	new WordPress_SEO();
}
