# Plugins

This folder contains all default functions and SASS (scss) styling for various free and commercial plugins.

**Do not change and/or adjust any code in this folder**. Adjustments and extensions need to be made within the master template files.
