<?php

namespace Plugins\Gravity_Forms;

class Gravity_Forms {


	public function __construct() {
		if ( class_exists( 'GFForms' ) ) {
			add_action( 'admin_init', array( $this, 'permissions' ) );
			add_filter( 'gform_ajax_spinner_url', array( $this, 'gravity_forms_spinner' ), 10, 2 );
		}
	}

	public function permissions() {
		$role = get_role( 'editor' );
		$role->add_cap( 'gform_full_access' );
	}

	public function gravity_forms_spinner( $src ) {
		// http://preloaders.net/en/popular
		return get_stylesheet_directory_uri() . '/plugins/gravityforms/img/gravityforms-spinner.svg';
	}
}

new Gravity_Forms();
