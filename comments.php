<?php
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="c-comments">
  <?php if ( have_comments() ) : ?>
	<h2 class="c-comments__title">
		<?php
		$theme_name_comment_count = get_comments_number();
		if ( '1' === $theme_name_comment_count ) {
			printf(
				esc_html__( 'One thought on &ldquo;%1$s&rdquo;', 'theme-boilerplate' ),
				'<span>' . get_the_title() . '</span>'
			);
		} else {
			printf(
				esc_html( _nx( '%1$s thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', $theme_name_comment_count, 'comments title', 'theme-boilerplate' ) ),
				number_format_i18n( $theme_name_comment_count ),
				'<span>' . get_the_title() . '</span>'
			);
		}
		?>
	</h2>
		<?php the_comments_navigation(); ?>
	<ol class="c-comments__list">
		<?php
		wp_list_comments(
			array(
				'walker'            => null,
				'max_depth'         => '',
				'style'             => 'ul',
				'callback'          => null,
				'end-callback'      => null,
				'type'              => 'all',
				'reply_text'        => __( 'Reply', 'theme-boilerplate' ),
				'page'              => '',
				'per_page'          => '',
				'avatar_size'       => 50,
				'reverse_top_level' => null,
				'reverse_children'  => '',
				'format'            => 'html5',
				'short_ping'        => false,
				'echo'              => true,
			)
		);
		?>
	</ol>

		<?php
		the_comments_navigation();
		if ( ! comments_open() ) :
			?>
	  <p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'theme-name' ); ?></p>
			<?php
		endif;
  endif;
  comment_form();
	?>
</div>
